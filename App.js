import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {Component} from 'react';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menu: '',
      harga: 0,
      jumlah: 0,
      total: 0,
    };
  }
  getTotal = () => {
    const {harga, jumlah} = this.state;
    if (this.state.menu === 'soto ayam') {
      this.setState({
        harga: 15000,
      });
    } else if (this.state.menu === 'soto kambing') {
      this.setState({
        harga: 25000,
      });
    } else if (this.state.menu === 'sate ayam') {
      this.setState({
        harga: 25000,
      });
    } else if (this.state.menu === 'sate kambing') {
      this.setState({
        harga: 35000,
      });
    } else {
      this.setState({
        harga: 0,
      });
    }

    const total = harga * jumlah;
    this.setState({total});
  };
  render() {
    const {menu, harga, jumlah, total} = this.state;
    return (
      <View style={{marginHorizontal: 10}}>
        <Text>MENU</Text>
        <Text>Soto Ayam - 15.000</Text>
        <Text>Soto Kambing - 25.000</Text>
        <Text>Sate Ayam - 25.000</Text>
        <Text>Sate Kambing - 35.000</Text>
        <Text>========================</Text>

        <Text>Pilih Menu</Text>
        <TextInput
          style={styles.input}
          onChangeText={menu => this.setState({menu})}
        />

        <Text>jumlah Pesanan</Text>
        <TextInput
          style={styles.input}
          onChangeText={jumlah => this.setState({jumlah})}
        />
        <TouchableOpacity
          onPress={() => this.getTotal()}
          style={{
            width: '100%',
            height: 50,
            borderRadius: 10,
            backgroundColor: '#000',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontSize: 20}}>
            Tampilkan Total Harga
          </Text>
        </TouchableOpacity>
        <View
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#000',
            }}>
            Total Harga
          </Text>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: '#000',
            }}>
            {total}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderColor: '#000',
    padding: 10,
    marginVertical: 20,
  },
});
